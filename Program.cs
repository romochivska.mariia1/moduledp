﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp14
{
    /*. Резюме користувача – це об’єкт, який може мiстити ПiБ, фотографiю, вiдомостi
    про освiту (ЗВО, спецiальнiсть, рiк отримання диплому), вiдомостi про
    попереднi мiсця роботи (компанiя, посада, рiк початку роботи та рiк звiльнення)
    та список вiдомих технологiй.
    Деякi данi в резюме можуть бути вiдсутнi.
    Створити декiлька об’єктiв резюме, наприклад: молодого спецiалiста, який
    закiнчив ЗВО в цьому роцi, без досвiду роботи, з фото та 1 вiдомою технологiєю
    та резюме досвiдченого сеньйора iз 2 вищими освiтами, стажем 15 рокiв в
    рiзних компанiях, без фото, та зi списком з 5 вiдомих технологiй.
    Вказати шаблон, який доцiльно використати для розв'язування задачi.
    */
   
    /*Використано шаблон Factory.Фабрика визначає загальний інтерфейс для створення об'єктів,
    а конкретні підкласи фабрики реалізують цей інтерфейс і створюють конкретні об'єкти і це  робить код більш гнучким
    і підходить для ситуацій, де потрібно створювати об'єкти різних типів
    і ця функціональність можеливо буде розширюватися
    */
    class Resume
    {
        public string FullName { get; set; }
        public Dictionary<string, string> Education { get; set; } = new Dictionary<string, string>();
        public List<Dictionary<string, string>> WorkExperience { get; set; } = new List<Dictionary<string, string>>();
        public List<string> Technologies { get; set; } = new List<string>();
        public string Photo { get; set; }

        public void AddEducation(string university, string specialty, string year)
        {
            Education["University"] = university;
            Education["Specialty"] = specialty;
            Education["Year"] = year;
        }

        public void AddWorkExperience(string company, string position, string startYear, string endYear)
        {
            WorkExperience.Add(new Dictionary<string, string>
            {
                { "Company", company },
                { "Position", position },
                { "StartYear", startYear },
                { "EndYear", endYear }
            });
        }

        public void AddTechnology(string technology)
        {
            Technologies.Add(technology);
        }

        public void AddPhoto(string photo)
        {
            Photo = photo;
        }

        public void Display()
        {
            Console.WriteLine("ПiБ: " + FullName);
            Console.WriteLine("Освiта: " + Education["University"] + ", " + Education["Specialty"] + ", " + Education["Year"]);
            Console.WriteLine("Досвiд роботи:");
            foreach (var experience in WorkExperience)
            {
                Console.WriteLine($"{experience["Company"]}, {experience["Position"]}, {experience["StartYear"]} - {experience["EndYear"]}");
            }
            Console.WriteLine("Технологiї: " + string.Join(", ", Technologies));
            Console.WriteLine("Фото: " + (string.IsNullOrEmpty(Photo) ? "Not Available" : "Available"));
        }
    }

    abstract class ResumeFactory
    {
        public abstract Resume CreateResume();

        public void DisplayResume()
        {
            var resume = CreateResume();
            resume.Display();
        }
    }

    class EntryLevelResumeFactory : ResumeFactory
    {
        public override Resume CreateResume()
        {
            var resume = new Resume
            {
                FullName = "iван Петренко",
                Photo = "Photo.jpg"
            };
            resume.AddEducation("УжНУ", "Молодий спецiалiст", "2023");
            resume.AddTechnology("Python");
            return resume;
        }
    }

    class SeniorResumeFactory : ResumeFactory
    {
        public override Resume CreateResume()
        {
            var resume = new Resume
            {
                FullName = "Юрiй Куцин"
            };
            resume.AddEducation("УжНУ", "Бакалавр", "2005");
            resume.AddEducation("УжНУ", "Магiстр", "2010");
            resume.AddWorkExperience("Компанiя A", "Senior Developer", "2005", "2010");
            resume.AddWorkExperience("Компанiя B", "Lead Developer", "2010", "2025");
            resume.AddTechnology("C#");
            resume.AddTechnology("SQL");
            resume.AddTechnology("JavaScript");
            return resume;
        }
    }

    class Program
    {
        static void Main()
        {
            Console.WriteLine("Резюме молодого спецiалiста:");
            var entryLevelFactory = new EntryLevelResumeFactory();
            entryLevelFactory.DisplayResume();

            Console.WriteLine("\n Резюме сеньйора:");
            var seniorFactory = new SeniorResumeFactory();
            seniorFactory.DisplayResume();
        }
    }
}
